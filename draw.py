import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

x = []
y = []
z = []
s24 = []
s25 = []
s26 = []
s27 = []
s28 = []
s29 = []
s30 = []
s31 = []
colors = []


with open('pre-em-data', 'r') as file:
    line = file.readline()
    while line:
        word = line.split()
        x.append(int(word[0]))
        y.append(int(word[1]))
        z.append(int(word[2]))
        s24.append(int(word[3]))
        s25.append(int(word[5]))
        s26.append(int(word[7]))
        s27.append(int(word[9]))
        s28.append(int(word[11]))
        s29.append(int(word[13]))
        s30.append(int(word[15]))
        s31.append(int(word[17]))
        eye = int(word[3]) + int(word[5]) + int(word[7]) + int(word[9]) \
               + int(word[11]) + int(word[13]) + int(word[15]) + int(word[17])
        if eye > 700 * 8:
            colors.append("red")
        elif eye > 600 * 8:
            colors.append("green")
        elif eye > 500 * 8:
            colors.append("black")
        else:
            colors.append("orange")

        line = file.readline()
 
 
fig = plt.figure()
ax = Axes3D(fig)
ax.scatter(x, y, z, c=colors)
 
plt.title("red: upper 700; green upper 600; black upper 500; orange less 500")
 
ax.set_zlabel('post', fontdict={'size': 15, 'color': 'red'})
ax.set_ylabel('main', fontdict={'size': 15, 'color': 'red'})
ax.set_xlabel('pre', fontdict={'size': 15, 'color': 'red'})
plt.show()
